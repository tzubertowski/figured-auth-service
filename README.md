#### About
This repository contains authentication service for Figured API test.

#### Dependencies
This project is based off laravel 5.6 and laravel passport. 
There is no custom functionality (everything comes pre-bundled with passport) except for database seeders, which explains lack of tests.

You can read more about passport [here](https://laravel.com/docs/5.6/passport).
