<?php

use Illuminate\Database\Seeder;

class PasswordGrantSeeder extends Seeder
{
    public function run()
    {
        DB::table('oauth_clients')->insert([
            'name' => 'password grant',
            'secret' => 'DJP2TGfrPkyZzDe80Ve3Sq4DZNpfVaB8lyvvn0D2',
            'redirect' => 'http://localhost',
            'personal_access_client' => 0,
            'password_client' => 1,
            'revoked' => 0,
        ]);
    }
}
