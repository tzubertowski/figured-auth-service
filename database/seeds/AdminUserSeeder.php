<?php

use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin',
            // password => 123qwe
            'password' => '$2y$10$GzXZBanKhJFjLU6HjU/g8.IwT0XOzuwBzcYwVYuLFfMW.pUp6DXba',
        ]);
    }
}
